// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "pw_Snake.generated.h"

class UCameraComponent;
UENUM()
enum class EMovementDirection
{
	UP,
    DOWN,
    LEFT,
    RIGHT,
	FORWARD,
	BACKWARD
};

UCLASS()
class SNAKEHW_API Apw_Snake : public APawn
{
	GENERATED_BODY()
private:
	EMovementDirection CameraDirection;
public:
	// Sets default values for this pawn's properties
	Apw_Snake();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void Test();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void CameraMovement(float DeltaTime);
	float step = -0.05;
	float AngleAxis;
	
};
