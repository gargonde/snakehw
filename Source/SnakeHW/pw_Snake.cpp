// Fill out your copyright notice in the Description page of Project Settings.


#include "pw_Snake.h"

#include <string>


#include "TimerManager.h"
#include "Engine/Engine.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "GameFramework/GameMode.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
Apw_Snake::Apw_Snake()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	
}

// Called when the game starts or when spawned
void Apw_Snake::BeginPlay()
{
	Super::BeginPlay();
	CameraDirection = EMovementDirection::UP;
	//SetActorRotation(FRotator(-90,0,0));

	AngleAxis = 0;

	

}
void Apw_Snake::Test()
{
	GEngine->AddOnScreenDebugMessage(1,10.f,FColor::Red,TEXT("Hello!"));
	CameraDirection =  EMovementDirection::UP;//static_cast<EMovementDirection>(static_cast<int>(CameraDirection) + 1);
}

// Called every frame
void Apw_Snake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CameraMovement(DeltaTime);
	
}

// Called to bind functionality to input
void Apw_Snake::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void Apw_Snake::CameraMovement(float DeltaTime)
{
	FVector curLoc = GetActorLocation();
	//
	// if(CameraDirection == EMovementDirection::FORWARD)		
	// 	curLoc = curLoc + GetActorForwardVector() * 100 * DeltaTime ;
	// if(CameraDirection == EMovementDirection::RIGHT)		
	// 	curLoc = curLoc + GetActorRightVector() * 100 * DeltaTime ;
	// if(CameraDirection == EMovementDirection::UP)		
	// 	curLoc = curLoc + GetActorUpVector() * 100 * DeltaTime ;
	// if(CameraDirection == EMovementDirection::DOWN)		
	// 	curLoc = curLoc + (GetActorUpVector()*-1) * 100 * DeltaTime ;
	// if(CameraDirection == EMovementDirection::LEFT)		
	// 	curLoc = curLoc + (GetActorRightVector()*-1) * 100 * DeltaTime ;
	// if(CameraDirection == EMovementDirection::BACKWARD)		
	// 	curLoc = curLoc + (GetActorForwardVector()*-1) * 100 * DeltaTime ;
	// SetActorLocation(curLoc);
	//
	//
	// //FVector CameraLocation =  GetActorLocation();
	FVector MyLocation = FVector(1430.,-30.,100.);
	//
	// //GEngine->AddOnScreenDebugMessage(1,10.f,FColor::Red,UKismetMathLibrary::FindLookAtRotation(curLoc,MyLocation).ToString());
	// FRotator rot = UKismetMathLibrary::FindLookAtRotation(curLoc,MyLocation);
	//
	// //SetActorRotation(FRotator(rot.Pitch,0,0));
	//
 // 	FRotator rotAct = GetActorRotation();

	// angle increases by 1 every frame
	AngleAxis++;

	// prevent number from growind indefinitely
	if(AngleAxis > 360.0f) {

		AngleAxis = 1;
	}
	FVector Radius = FVector(200,0,0);
	FVector RotateValue = Radius.RotateAngleAxis(AngleAxis, FVector (1,0,0));

	MyLocation.X += RotateValue.X;
	MyLocation.Y += RotateValue.Y;
	MyLocation.Z += RotateValue.Z;
	
	SetActorLocation(MyLocation);
	
	// if (rotAct.Pitch <= -90)
	// {
	// 	GEngine->AddOnScreenDebugMessage(1,10.f,FColor::Red,rotAct.ToString());
	// 	SetActorRotation(FRotator (0,rotAct.Yaw,rotAct.Roll));
	// }
		
	
	
}
