// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeHWGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEHW_API ASnakeHWGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
